package ru.krg;

import java.util.ArrayList;
import java.util.List;

public class Bank {
	
	public static void main(String[] args) throws InterruptedException {
		Back back = new Back();
		Front front = new Front();
		
		System.out.println("Понеслась!");
		//	Создаём клиентов и запускаем их 
		List<Client> clients = new ArrayList<>();
		for(int i=0; i<5; i++){
			Client client = new Client("Клиент №"+i, front);
			clients.add(client);
			client.setSumm((long) (Math.random()*10000));
			client.start();
		}
		// Создаём обработчики и запускаем
		List<Proc> procs = new ArrayList<>();
		for(int i=0; i<2; i++){
			Proc proc = new Proc("Обработчик №"+i, front, back);
			procs.add(proc);
			proc.start();
		}
		// Ждём пока клиенты отработают
		for(Client client: clients){
			client.join();
		};
		//	Как клиенты отработали тормазим обработчики
		for(Proc proc: procs){
			proc.interrupt();
		};
	}
	
}

