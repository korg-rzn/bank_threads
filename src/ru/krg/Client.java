package ru.krg;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Client extends Thread{
	private long summ = 0;
	private Front front;
	
	public Client(String name, Front front){
		super(name);
		this.front = front;
	}
	public void run(){
		// создаём заявку и пихаем её во фронт
		Request request = new Request(this, (long)(Math.random()*this.getSumm()), Math.random()>0.5);
		System.out.println("\""+this.getName()+"\" создан запрос ("+(request.isDebit()?"Дебит":"Кредит")+" "+request.getSumm()+")");
		try {
			front.putRequest(request);
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}
	}
	// гетеры и сетеры
	public long getSumm() {
		return summ;
	}

	public void setSumm(long summ) {
		this.summ = summ;
	}
	// изменение балнса клиента 
	public void plus(long summ){
		this.summ += summ;
	}
	
	public void minus(long summ) throws Exception{
		if(this.summ < summ){
			throw new Exception("Не достаточно средств у клиента");
		}
		this.summ -= summ;
	}
	
}
