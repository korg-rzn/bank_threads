package ru.krg;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Back {
	long summ = 0;
	
	private void plus(long summ){
		this.summ += summ;
	}
	
	private void minus(long summ) throws Exception{
		if(this.summ < summ){
			throw new Exception("Не достаточно средств в банке");
		}
		this.summ -= summ;
	}
	
	public synchronized void putRequest(Request request){
		StringBuilder pr = new StringBuilder();	// для вывода сообщения
		pr.append("Back: ");
		long summ = request.getSumm();	// сумма запроса
		Client client = request.getClient();
		pr.append("Заявка {Клиент:"+client.getName()+"; сумма:"+summ+"; операция:"+(request.isDebit()?"дебет":"кредит")+"} - ");
		boolean error = false;	// признак ошибки
		StringBuilder errorText = new StringBuilder();	// сообщение об ошибке
		if(request.isDebit()){	// дебет
			try {
				client.minus(summ);	// ошибка если денег у клиента не достаточно
				this.plus(summ);
			} catch (Exception e) {
				error = true;
				errorText.append(e.getMessage());
			};
		}else{	// кредит
			try{
				this.minus(summ);	// ошибка, если денег в банке недостаточно
				client.plus(summ);
			}catch(Exception e){
				error = true;
				errorText.append(e.getMessage());
			};
		};
		if(error){
			pr.append("ОТКАЗАНО: " + errorText.toString());
		}else{
			pr.append("УСПЕШНО");
		};
		pr.append(" Обработчик: "+request.getProc().getName()+"; ");
		pr.append("Баланс: "+this.summ);
		System.out.println(pr.toString());
	}
}
