package ru.krg;

public class Request {
	private boolean debit = false;	//	false - забираем из банка, true - кладём в банк
	private long summ = 0;
	private Client client;
	private Proc proc;
	//	Конструкторы
	public Request(Client client, long summ){
		this.setClient(client);
		this.setSumm(summ);
	}
	public Request(Client client, long summ, boolean debit){
		this.setClient(client);
		this.setSumm(summ);
		this.setDebit(debit);
	}
	//	Геттеры и сеттеры
	public void setDebit(boolean debit){
		this.debit = debit;
	}
	public boolean isDebit(){
		return this.debit;
	}

	public long getSumm() {
		return summ;
	}

	public void setSumm(long summ) {
		this.summ = summ;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Proc getProc() {
		return proc;
	}

	public void setProc(Proc proc) {
		this.proc = proc;
	}
	public String getType(){
		return this.isDebit()?"дебет":"кредит";
	}

}
