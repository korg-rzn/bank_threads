package ru.krg;

public class Proc extends Thread{
	private Front front;
	private Back back;
	
	public Proc(String name, Front front, Back back){
		super(name);
		this.front = front;
		this.back = back;
	}
	public void run(){
		// выполняем, пока не тормазнули или пока во фронте есть заявки
		while(front.size()>0 || !isInterrupted()){
			Request request = front.getRequest();
			if(request!=null){	// если из фронта получена заявка
				System.out.println("\""+this.getName()+"\": получена заявка от "+request.getClient().getName()+" ");
				request.setProc(this);
				// пихаем её в бек
				back.putRequest(request);
			};
		};
	}
}
