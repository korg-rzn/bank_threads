package ru.krg;

import java.util.ArrayDeque;

public class Front extends ArrayDeque<Request>{
	// добавить заявку
	public synchronized void putRequest(Request request) throws InterruptedException{
		while(this.size()>1){ // если заявок больше 2 в очереди, то ждём пока освободится
			wait();	
		};
		this.add(request);
	}
	// забрать заявку
	public synchronized Request getRequest(){
		Request request = this.poll();
		if(request!=null){
			notifyAll(); // очередь освободили, даём знать об этом клиентам
		};
		return request;
	}
}
